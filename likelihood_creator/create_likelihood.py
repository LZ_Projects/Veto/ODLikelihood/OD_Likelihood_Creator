import numpy as np
import matplotlib.pyplot as plt
from scipy import signal


def calculate_likelihood(sig, bg):
    """
    :param sig: signal values
    :type sig: np.array[float]
    :param bg: background values
    :type bg: np.array[float]
    :return: likelihood values
    :rtype: np.array[float]
    """

    likelihood = sig / (bg + sig)
    return likelihood


def create_likelihood_distribution(signal_weights, bg_weights, bins, signal_filter, bg_filter, show_distros=True):
    """

    :param signal_weights: location of the signal variable
    :type signal_weights: np.array[float]
    :param bg_weights: location of the background variable
    :type bg_weights: np.array[float]
    :param bins:
    :type bins: np.array[int] or np.array[float]
    :param signal_filter: signal savgol window and order
    :type signal_filter: list[int, int]
    :param bg_filter: background savgol window and order
    :type bg_filter: list[int, int]
    :param show_distros: if True, run plot_distributions to show plots
    :type show_distros: bool
    :return:
    :rtype:
    """

    # Smooth distribution
    smoothed_signal = signal.savgol_filter(signal_weights, signal_filter[0], signal_filter[1])
    smoothed_bg = signal.savgol_filter(bg_weights, bg_filter[0], bg_filter[1])

    # Normalise
    signal_weights = signal_weights / sum(signal_weights)
    bg_weights = bg_weights / sum(bg_weights)
    smoothed_signal = smoothed_signal / sum(smoothed_signal)
    smoothed_bg = smoothed_bg / sum(smoothed_bg)

    likelihood = calculate_likelihood(signal_weights, bg_weights)
    smoothed_likelihood = calculate_likelihood(smoothed_signal, smoothed_bg)

    if show_distros:
        plot_distributions(signal_weights, bg_weights, smoothed_signal, smoothed_bg, likelihood, smoothed_likelihood,
                           bins)

    return likelihood, smoothed_likelihood


def plot_distributions(raw_sig, raw_bg, smooth_sig, smooth_bg, raw_l, smooth_l, bins):
    fig, ax = plt.subplots(2, 2, figsize=(20, 20))
    ax[0, 0].plot(bins[0:-1], raw_sig, label='Signal')
    ax[0, 0].plot(bins[0:-1], raw_bg, label='Background')
    ax[0, 0].set_title('Raw distributions', fontsize=20)
    ax[0, 1].plot(bins[0:-1], smooth_sig, label='Signal')
    ax[0, 1].plot(bins[0:-1], smooth_bg, label='Background')
    ax[0, 1].set_title('Smoothed distributions', fontsize=20)
    ax[1, 0].plot(bins[0:-1], raw_l)
    ax[1, 0].set_title('Raw Likelihood', fontsize=20)
    ax[1, 1].plot(bins[0:-1], smooth_l)
    ax[1, 1].set_title('Smoothed Likelihood', fontsize=20)
    for axis in ['top', 'bottom', 'left', 'right']:
        ax[0, 0].spines[axis].set_linewidth(1.5)
        ax[0, 1].spines[axis].set_linewidth(1.5)
        ax[1, 0].spines[axis].set_linewidth(1.5)
        ax[1, 1].spines[axis].set_linewidth(1.5)
    ax[0, 0].legend(fontsize=20)
    ax[0, 1].legend(fontsize=20)
    plt.show()
