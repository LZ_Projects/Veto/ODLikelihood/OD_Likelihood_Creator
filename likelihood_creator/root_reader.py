import uproot as up
import matplotlib.pyplot as plt
import numpy as np


def load_histogram(file, hist_path):
    up_file = up.open(file)
    weights, bins = up_file[hist_path].to_numpy()

    return weights, bins


def view_histogram_1d(weights, bins, logy=False, labels=['signal', 'background'], norm=True):

    fig, ax = plt.subplots(1, 1, figsize=(20, 10))

    if type(weights) == list:
        for i, w in enumerate(weights):
            try:
                label = labels[i]
            except:
                label='weight ' + str(i)
            if norm:
                w = w/sum(w)
            ax.hist(bins[:-1],bins=bins, weights=w, label=label, histtype='step')
    else:
        if norm:
            weights = weights / sum(weights)
        ax.hist(bins[:-1], bins=bins, weights=weights)
    if logy:
        ax.set_yscale('log')
    plt.show()


